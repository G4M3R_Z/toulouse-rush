﻿using UnityEngine;

public class SpawnObs : MonoBehaviour
{
    private SpawnProgresion _spawn;
    private SpawnRoad _road;
    private int _spawnNum, _secuence;
    
    private void Awake()
    {
        _spawn = GetComponentInParent<SpawnProgresion>();
        _road = GetComponentInParent<SpawnRoad>();

        if (_road._colorRoad == 1 && _road._addRoad)
        {
            for (_secuence = 0; _secuence < 1; _secuence++)
            {
                _spawnNum = Random.Range(0, _spawn._obsLimit);

                if (_road._roadSection > 0 && _spawnNum < 5)
                    CreateObs(_spawnNum, _road._lastObs);
                else if (_road._roadSection == 0)
                    CreateObs(_spawnNum, _road._lastObs);
                else
                    _secuence--;
            }
            _road._lastObs = _spawnNum;
        }

        Destroy(this);
    }
    void CreateObs(int num, int repeat)
    {
        if (num != repeat)
            Instantiate(_spawn._dropElements[num], transform);
        else
            _secuence--;
    }
}