﻿using UnityEngine;

public class AddScorePoints : MonoBehaviour
{
    public float _distance;
    public GameObject _particles, _icon;
    private Score _score;
    private void Awake()
    {
        int pos = Random.Range(-1, 2);
        Vector3 localPos = transform.localPosition;
        transform.localPosition = new Vector3(localPos.x, localPos.y, _distance * pos);
        _score = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Score>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _score._score += 100;
            Instantiate(_particles, other.transform);
            Instantiate(_icon, other.transform);
            Destroy(this.gameObject);
        }
    }
}