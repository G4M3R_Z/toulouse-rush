﻿using UnityEngine;

public class SetCamPosition : MonoBehaviour
{
    public GameObject _target, _cam;
    [Range(0,10)]
    public float _y, _z, _speed;
    [Range(0,90)]
    public float _rotation;

    private Movement _moves;

    private void Start()
    {
        _moves = _target.GetComponent<Movement>();
    }
    private void Update()
    {
        SetNormalCam(_target.transform.position);
        SetRot(_target.transform.localEulerAngles);
    }
    void SetNormalCam(Vector3 targetPos)
    {
        if (!_moves._jump)
            transform.position = targetPos;
        else
            transform.position = new Vector3(targetPos.x, transform.position.y, targetPos.z);

        _cam.transform.localPosition = new Vector3(0, _y, -_z);
        _cam.transform.localEulerAngles = new Vector3(_rotation, 0, 0);
    }
    void SetRot(Vector3 targetRot)
    {
        transform.rotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(targetRot), Time.deltaTime * _speed);
    }
}