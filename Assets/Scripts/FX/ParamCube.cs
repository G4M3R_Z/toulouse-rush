﻿using UnityEngine;

public class ParamCube : MonoBehaviour {

    public int _band;
    private float _startScale;
    public float _maxScale = 0.3f;

    private void Awake()
    {
        _band = Random.Range(0, 8);
        _startScale = transform.localScale.y;
    }
    void Update ()
    {
        Vector3 scale = transform.localScale;
        transform.localScale = new Vector3(scale.x, (AudioPeer._bandBuffer[_band] * _maxScale) + _startScale, scale.z);
    }
}