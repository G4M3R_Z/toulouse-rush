﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SetGameInStart : MonoBehaviour
{
    private GameObject _player;
    private SpawnRoad _world;
    private float _alpha;
    public List<Image> _images;
    public List<TextMeshProUGUI> _textColor;

    private void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _world = GameObject.FindGameObjectWithTag("World").GetComponent<SpawnRoad>();
        _player.SetActive(false);
        _world.enabled = false;

        SetColors();
    }
    private void Update()
    {
        _alpha = (_alpha < 1) ? _alpha += Time.deltaTime : _alpha = 1;
        SetColors();

        if (_alpha == 1)
        {
            _player.SetActive(true);
            _world.enabled = true;
            Destroy(this);
        }
    }
    void SetColors()
    {
        for (int i = 0; i < _textColor.Count; i++)
        {
            Color newColor = _textColor[i].color;
            newColor.a = _alpha;
            _textColor[i].color = newColor;
        }
        for (int i = 0; i < _images.Count; i++)
        {
            Color newColor = _images[i].color;
            newColor.a = _alpha;
            
            if(_images[i] != null)
                _images[i].color = newColor;
        }
    }
}