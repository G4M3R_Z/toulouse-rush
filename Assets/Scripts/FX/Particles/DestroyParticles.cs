﻿using UnityEngine;

public class DestroyParticles : MonoBehaviour
{
    private ParticleSystem _ps;

    private void Awake()
    {
        _ps = GetComponent<ParticleSystem>();
    }
    void Update()
    {
        if (_ps.isStopped)
            Destroy(this.gameObject);
    }
}