﻿using UnityEngine;

public class TunelMoves : MonoBehaviour
{
    [HideInInspector] public float _maxPos;
    [HideInInspector] public int _band;
    private float _startPos;

    private void Awake()
    {
        _startPos = transform.localPosition.y;
    }
    private void Update()
    {
        Vector3 pos = transform.localPosition;
        transform.localPosition = new Vector3(pos.x, (AudioPeer._bandBuffer[_band] * _maxPos) + _startPos, pos.z);
    }
}