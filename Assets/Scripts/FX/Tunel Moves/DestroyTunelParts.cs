﻿using UnityEngine;

public class DestroyTunelParts : MonoBehaviour
{
    public float _destroyDistance;
    private Transform _cam;

    private void Awake()
    {
        _cam = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }
    void Update()
    {
        float posZ = transform.position.z;
        if (posZ <= _cam.position.z - _destroyDistance)
            Destroy(this.gameObject);
    }
}