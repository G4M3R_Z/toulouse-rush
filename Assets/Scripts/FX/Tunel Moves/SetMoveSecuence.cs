﻿using System.Collections.Generic;
using UnityEngine;

public class SetMoveSecuence : MonoBehaviour
{
    [Range(0, 1)] public float _maxMoves;
    List<TunelMoves> _moves;

    private void Awake()
    {
        _moves = new List<TunelMoves>();

        for (int i = 0; i < transform.childCount; i++)
        {
            _moves.Add(transform.GetChild(i).GetComponent<TunelMoves>());
            _moves[i]._band = i;
            _moves[i]._maxPos = _maxMoves;
        }

        Destroy(this);
    }
}