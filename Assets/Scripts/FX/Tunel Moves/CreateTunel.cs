﻿using System.Collections.Generic;
using UnityEngine;

public class CreateTunel : MonoBehaviour
{
    public GameObject _tunel, _lights, _arc;
    public int _tunelLenth, _arcLenth;
    public float _separation;
    private BoxCollider[] _inAndOut;
    private void Awake()
    {
        float lenthBetween = 0;
        float shortBetween = 0;

        _inAndOut = GetComponents<BoxCollider>();
        Vector3 pos = new Vector3(0, 2.5f, 0);

        for (int i = 0; i < _inAndOut.Length; i++)
        {
            _inAndOut[i].center = pos;
            pos.z += _separation * _tunelLenth;
        }

        for (int i = 0; i < _tunelLenth; i++)
        {
            Buider(_tunel, 0, lenthBetween);
            Buider(_lights, 1, lenthBetween);

            for (int e = 0; e < _arcLenth; e++)
            {
                shortBetween += _separation / _arcLenth;
                Buider(_arc, 2, shortBetween);
            }

            lenthBetween += _separation;
        }
    }
    private void Update()
    {
        if (transform.GetChild(0).childCount == 0)
            Destroy(this.gameObject);
    }
    void Buider(GameObject build, int child, float separation)
    {
        GameObject builded = Instantiate(build, transform.GetChild(child)) as GameObject;
        builded.transform.localPosition = new Vector3(0, 0, separation);
    }
}