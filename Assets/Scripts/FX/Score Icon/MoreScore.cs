﻿using TMPro;
using UnityEngine;

public class MoreScore : MonoBehaviour
{
    public float _speed;
    private TextMeshPro _text;
    private Color _textColor;
    private void Awake()
    {
        _text = GetComponent<TextMeshPro>();
        _textColor = Color.white;
        _textColor.a = 1;
        _text.color = _textColor;

        transform.localPosition = new Vector3(0, 0, -0.5f);
    }
    private void Update()
    {
        transform.Translate(0, _speed * Time.deltaTime, 0);
        _textColor.a -= Time.deltaTime * 2;
        _text.color = _textColor;

        if (_textColor.a <= 0)
            Destroy(this.gameObject);
    }
}