﻿using UnityEngine;

public class PlayerStartEffect : MonoBehaviour
{
    [Range(0,10)]
    public float _backDistance, _speed;
    private void Awake()
    {
        transform.localPosition = new Vector3(0, 0, -_backDistance);
    }
    private void Update()
    {
        Vector3 pos = Vector3.Lerp(transform.localPosition, Vector3.zero, Time.deltaTime * _speed);
        transform.localPosition = pos;

        if(Mathf.Abs(pos.z) < 0.01f)
        {
            transform.localPosition = Vector3.zero;
            Destroy(this);
        }
    }
}