﻿using UnityEngine;

public class EmmisionMusic : MonoBehaviour
{
    public Material _emmisinMat;
    
    [Range(0, 8)] public int _band;
    [Range(0, 10)] public float _minEmmision, _maxEmmisive;

    private void Update()
    {
        float emission = Mathf.Clamp(AudioPeer._bandBuffer[_band], _minEmmision, _maxEmmisive);
        Color finalColor = Color.white * Mathf.LinearToGammaSpace(emission);
        _emmisinMat.SetColor("_EmissionColor", finalColor);
    }
}