﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    public bool _firstFade, _canDestoy;
    public string sceneName;
    private Image _pannel;
    private Color _newColor;
    [HideInInspector]
    public float _alpha;

    private void Start()
    {
        _pannel = GetComponent<Image>();
        _alpha = (_firstFade) ? _alpha = 1 : _alpha = 0;
        _newColor.a = _alpha;
        _pannel.color = _newColor;
    }
    private void Update()
    {
        FadeControl();

        if (_alpha >= 1)
            SceneManager.LoadScene(sceneName);

        if (_alpha <= 0 && _canDestoy)
            Destroy(this.gameObject);
    }
    void FadeControl()
    {
        _alpha = (_firstFade) ? _alpha -= Time.deltaTime : _alpha += Time.deltaTime;
        _alpha = Mathf.Clamp(_alpha, 0, 1);
        _newColor.a = _alpha;
        _pannel.color = _newColor;
    }
}