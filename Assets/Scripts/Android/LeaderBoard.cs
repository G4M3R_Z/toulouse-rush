﻿using UnityEngine;

public class LeaderBoard : MonoBehaviour
{
    private void Awake()
    {
        PostLeaderBoard();
    }
    void PostLeaderBoard()
    {
        long scoreToPost = (int)PlayerPrefs.GetFloat("HighScore");
        SingIn.PostToLeaderBoard(scoreToPost);
    }
    public void ShowLeaderBoard()
    {
        SingIn.ShowLeaderboardUI();
    }
}