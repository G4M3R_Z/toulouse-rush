﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using TMPro;

public class SingIn : MonoBehaviour
{
    private TextMeshProUGUI _text;
    private bool _succed;
    private void Start()
    {
        _text = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        StartCoroutine(AuthenticateUser());
    }
    IEnumerator AuthenticateUser()
    {
        while (!_succed)
        {
            _text.text = "Singing into" + "\n" + "Google Play Acount";
            _text.color = Color.white;

            yield return new WaitForSeconds(1f);

            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.Activate();

            yield return Social.localUser.authenticated;

            Social.localUser.Authenticate((bool succed) =>
            {
                if (succed)
                {
                    _succed = true;
                    SceneManager.LoadScene(1);
                }
                else
                {
                    _text.text = "Could not login to" + "\n" + "Google Play Acount";
                    _text.color = Color.red;
                }
            });

            yield return new WaitForSeconds(1f);
        }
    }
    public static void PostToLeaderBoard(long newScore)
    {
        Social.ReportScore(newScore, GPGSIds.leaderboard_highscore, (bool succed) =>
        {
            if (succed)
                Debug.Log("Posted new score to leaderboard");
            else
                Debug.LogError("Unable to post new score to leaderboard");
        });
    }
    public static void ShowLeaderboardUI()
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_highscore);
    }
}