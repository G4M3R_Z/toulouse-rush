﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [Range(0,10)]
    public float _speed;
    [HideInInspector]
    public float _score;

    public List<TextMeshProUGUI> _texts;
    public GameObject _player;

    private void Awake()
    {
        _texts[0].text = _score.ToString("0");
        _texts[1].text = PlayerPrefs.GetInt("HighScore").ToString();
    }
    private void Update()
    {
        if (_player.activeSelf)
        {
            _score += Time.deltaTime * _speed;
            _texts[0].text = _score.ToString("0");
        }

        if (_score > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", (int)_score);
            _texts[1].text = _score.ToString("0");
        }
    }
}