﻿using UnityEngine;
using UnityEngine.UI;

public class ResetSlider : MonoBehaviour
{
    private bool _isUsed;
    private Slider _slideValue;
    private void Start()
    {
        _slideValue = GetComponent<Slider>();
    }
    void Update()
    {
        if (!_isUsed)
        {
            _slideValue.value = Mathf.Lerp(_slideValue.value, 0, Time.deltaTime * 5);

            if (_slideValue.value == 0)
                _isUsed = true;
        }
    }
    public void SelectSlide(bool active)
    {
        _isUsed = active;
    }
}