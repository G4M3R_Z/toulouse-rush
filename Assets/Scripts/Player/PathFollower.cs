﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class PathFollower : MonoBehaviour
{
    public float _speed;
    private float _velocity;
    public PathCreator _path;
    private AutoValace _valance;

    private void Awake()
    {
        _valance = GetComponent<AutoValace>();
    }
    void FixedUpdate()
    {
        _velocity += _speed * Time.deltaTime;

        Vector3 move = _path.path.GetPointAtDistance(_velocity);
        transform.position = new Vector3(move.x, transform.position.y, move.z);

        Vector3 direction = _path.path.GetRotationAtDistance(_velocity).eulerAngles;
        //_valance.Valance(transform.position, direction.y);
    }
}