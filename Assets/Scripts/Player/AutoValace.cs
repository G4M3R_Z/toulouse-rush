﻿using UnityEngine;

public class AutoValace : MonoBehaviour
{
    [Range(0, 10)] public float _rayDistance;
    [Range(0, 10)] public float _smothSpeed;
    public LayerMask _layerDetection;

    public void Valance(Vector3 playerPos, float slide, float radius, bool jump)
    {
        Ray ray = new Ray(playerPos, -transform.up * _rayDistance);
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * _rayDistance);
        RaycastHit hitInfo;
        
        if (Physics.Raycast(ray, out hitInfo, _rayDistance, _layerDetection, QueryTriggerInteraction.Ignore))
        {
            //Set Horizontal Moves
            Vector3 h = new Vector3(slide, 0, playerPos.z);
            h.y = (!jump) ? h.y = hitInfo.point.y + radius : h.y = playerPos.y;
            transform.position = h;

            //Get up position and smoth vector speed
            Quaternion getRot = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
            Quaternion setRot = Quaternion.Lerp(transform.localRotation, getRot, Time.deltaTime * _smothSpeed);

            //Set smoth Rotation
            transform.rotation = setRot;
        }
    }
}