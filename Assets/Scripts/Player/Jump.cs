﻿using UnityEngine;

public class Jump : MonoBehaviour
{
    [Range(0,10)]
    public float _jumpForce;
    public LayerMask _detectJump;
    private Movement _moves;

    private void Awake()
    {
        _moves = GetComponent<Movement>();   
    }
    public void JumpDetector(float speed, Rigidbody rgb)
    {
        float distance = speed / 2;
        Ray ray = new Ray(transform.position, transform.forward * distance);
        Debug.DrawLine(ray.origin, ray.origin + ray.direction * distance);

        if (Physics.Raycast(ray, distance, _detectJump, QueryTriggerInteraction.Ignore))
        {
            rgb.useGravity = _moves._jump = true;
            //rgb.constraints = RigidbodyConstraints.FreezePositionX;
            rgb.AddForce(Vector3.up * _jumpForce * 50);
        }
    }
}