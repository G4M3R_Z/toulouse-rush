﻿using UnityEngine;

public class SpinBall : MonoBehaviour
{
    private SpeedController _speed;
    private void Awake()
    {
        _speed = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInParent<SpeedController>();
    }
    private void Update()
    {
        transform.Rotate(_speed.currentSpeed / 3, 0, 0);
    }
}