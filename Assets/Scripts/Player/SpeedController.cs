﻿using System.Collections;
using UnityEngine;

public class SpeedController : MonoBehaviour
{
    [Range(0,10)]
    public float _changeSpeed;
    [Range(0, 100)]
    public float _startSpeed, _mediumSpeed, _maxSpeed;
    [Range(0, 30)]
    public float _fstSTime, _secSTime;
    [HideInInspector]
    public float currentSpeed;
    private GameObject _player;

    private void Awake()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        currentSpeed = _startSpeed;
        StartCoroutine("SpeedValue");
    }
    IEnumerator SpeedValue()
    {
        //esperar hasta que el player este listo
        yield return new WaitUntil(() => _player.activeSelf == true);

        float divider = 0;
        for (int i = 0; i <= _fstSTime * _changeSpeed; i++)
        {
            yield return new WaitForSeconds(1f / _changeSpeed);
            divider = (_mediumSpeed - _startSpeed) / _fstSTime;
            currentSpeed += divider / _changeSpeed;
        }
        for (int i = 0; i <= _secSTime * _changeSpeed; i++)
        {
            yield return new WaitForSeconds(1f / _changeSpeed);
            divider = (_maxSpeed - _mediumSpeed) / _secSTime;
            currentSpeed += divider / _changeSpeed;
        }
    }
}