﻿using UnityEngine;

public class DeadGame : MonoBehaviour
{
    public GameObject _deadUiScreen;
    public GameObject _deadParticles;
    private GameObject _canvas;
    private SetCamPosition _camPos;

    private void Start()
    {
        _canvas = GameObject.FindGameObjectWithTag("Canvas");
        _camPos = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInParent<SetCamPosition>();
    }
    void DestroyPlayer()
    {
        _camPos.enabled = false;
        Instantiate(_deadUiScreen, _canvas.transform);
        Instantiate(_deadParticles, transform.position, transform.rotation);
        Destroy(this.gameObject);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Finish"))
            DestroyPlayer();
    }
}