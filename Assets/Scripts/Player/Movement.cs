﻿using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{
    [Range(0, 5)] public float _bLimit;
    [HideInInspector] public bool _jump;
    private float _slideMove;

    private bool _insideTunel;
    private SpeedController _speed;
    private Rigidbody _rgb;
    private SphereCollider _collider;
    private AutoValace _autoValance;
    private Jump _jumpDetector;

    private Slider _slide;

    private void Awake()
    {
        _speed = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInParent<SpeedController>();
        _rgb = GetComponent<Rigidbody>();
        _collider = GetComponent<SphereCollider>();
        _autoValance = GetComponent<AutoValace>();
        _jumpDetector = GetComponent<Jump>();

        _slide = GameObject.FindGameObjectWithTag("GameController").GetComponent<Slider>();
        _slide.maxValue = _bLimit;
        _slide.minValue = -_bLimit;

        _jump = false;
    }
    private void Update()
    {
        if(!_jump)
            _jumpDetector.JumpDetector(_speed.currentSpeed, _rgb);
    }
    private void FixedUpdate()
    {
        _slideMove = Mathf.Lerp(_slideMove, _slide.value, Time.deltaTime * 20);
        _autoValance.Valance(transform.position, _slideMove, _collider.radius, _jump);

        if (!_jump)
            _rgb.velocity = transform.forward * _speed.currentSpeed;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (_jump)
        {
            _rgb.useGravity = false;
            _rgb.velocity = new Vector3(_rgb.velocity.x, 0, _rgb.velocity.z);
            //_rgb.constraints = ~RigidbodyConstraints.FreezePositionZ;
            _jump = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("InsideTunel"))
        {
            _insideTunel = !_insideTunel;
            _speed.currentSpeed = (_insideTunel) ? _speed.currentSpeed += 8 : _speed.currentSpeed -= 8;
        }
    }
}