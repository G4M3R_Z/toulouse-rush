﻿using UnityEngine;
using UnityEngine.UI;

public class DestroyTutorial : MonoBehaviour
{
    private Slider _value;

    private void Start()
    {
        _value = GetComponentInParent<Slider>();
    }
    private void Update()
    {
        if (_value.value != 0)
            Destroy(this.gameObject);
    }
}