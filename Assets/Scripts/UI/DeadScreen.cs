﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DeadScreen : MonoBehaviour
{
    public GameObject _fade;
    private Fade _fdCtrl;
    public List<TextMeshProUGUI> _texts;
    private float _alpha;

    private Transform _canvas;
    private List<GameObject> _ui;
    private Score _score;

    private void Awake()
    {
        _canvas = GameObject.FindGameObjectWithTag("Canvas").transform;
        _score = _canvas.GetComponent<Score>();
        _ui = new List<GameObject>();

        DestroyUIElements();
        SetScores();
        ShowTexts(_alpha);
    }
    private void Update()
    {
        _alpha = (_alpha < 1) ? _alpha += Time.deltaTime : _alpha = 1;
        ShowTexts(_alpha);
    }
    void DestroyUIElements()
    {
        for (int i = 0; i < _canvas.childCount - 1; i++)
            _ui.Add(_canvas.GetChild(i).gameObject);
        for (int i = 0; i < _ui.Count; i++)
            Destroy(_ui[i].gameObject);
    }
    void SetScores()
    {
        _texts[0].text = PlayerPrefs.GetInt("HighScore").ToString();
        _texts[2].text = _score._score.ToString("0");
        Destroy(_score);
        PlayerPrefs.Save();
    }
    void ShowTexts(float alpha)
    {
        Color newColor = new Color(0, 0, 0, alpha);
        
        for (int i = 0; i < _texts.Count; i++)
            _texts[i].color = newColor;
    }
    public void ChangeScene(string sceneName)
    {
        GameObject fade = Instantiate(_fade, _canvas) as GameObject;
        _fdCtrl = fade.GetComponent<Fade>();
        _fdCtrl._firstFade = false;
        _fdCtrl.sceneName = sceneName;
    }
}