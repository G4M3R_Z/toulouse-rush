﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    private bool _play;
    private List<Transform> _uiElements;
    private string _sceneName;

    private void Awake()
    {
        _uiElements = new List<Transform>();

        for (int i = 0; i < transform.childCount - 1; i++)
            _uiElements.Add(transform.GetChild(i));
    }
    private void Update()
    {
        float heigh = 1920;

        if (_play)
        {
            for (int i = 0; i < _uiElements.Count; i++)
            {
                float YPos = Mathf.Lerp(_uiElements[i].localPosition.y, heigh, Time.deltaTime * 2);
                _uiElements[i].localPosition = new Vector3(0, YPos, 0);
                heigh = -heigh;

                if (Mathf.Abs(heigh) - Mathf.Abs(YPos) < heigh / 10) //antes / 2 para solo la mitad
                    SceneManager.LoadScene(_sceneName);
            }
        }
    }
    public void PlayButton(string newScene)
    {
        _sceneName = newScene;
        _play = true;
    }
    public void OpenWebPage(string url)
    {
        Application.OpenURL(url);
    }
}