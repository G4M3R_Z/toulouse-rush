﻿using System.Collections.Generic;
using UnityEngine;

public class SetPilarsRot : MonoBehaviour
{
    public GameObject _scorePoint;
    private RoadEffectController _roadEfect;
    private SpawnRoad _road;
    private CloseSection _closeSection;
    List<Transform> _pilars;

    private void Awake()
    {
        _roadEfect = GetComponentInParent<RoadEffectController>();
        _road = GetComponentInParent<SpawnRoad>();
        _closeSection = GetComponentInParent<CloseSection>();

        SetRot();

        int randomizer = Random.Range(0, 6);
        if (randomizer != 3 || !_road._addRoad || _road._colorRoad == 1)
            Destroy(_scorePoint);

        if (_closeSection._isTunnel)
            foreach (Transform pilar in _pilars)
                Destroy(pilar.gameObject);

        Destroy(this);
    }
    void SetRot()
    {
        _pilars = new List<Transform>();

        for (int i = 0; i < transform.childCount - 1; i++)
        {
            _pilars.Add(transform.GetChild(i));

            float x = Random.Range(-_roadEfect._pilarRotX, _roadEfect._pilarRotX);
            float y = Random.Range(-_roadEfect._pilarRotY, _roadEfect._pilarRotY);
            float z = Random.Range(-_roadEfect._pilarRotZ, _roadEfect._pilarRotZ);

            _pilars[i].localEulerAngles += new Vector3(x, y, z);
        }
    }
}