﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRoad : MonoBehaviour
{
    [Range(0,1)] public float _buildTime;
    [Range(2,30)] public int _lenthLimit, _destroyDistance;

    public List<GameObject> _straight, _upDown, _twistLeftRight;

    private GameObject _player;
    [HideInInspector] public Transform _cam, _newSpawnPoint;
    [HideInInspector] public bool _addRoad;
    [HideInInspector] public int _roadSection, _colorRoad, _lastObs;

    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _cam = GameObject.FindGameObjectWithTag("MainCamera").transform;
        _newSpawnPoint = transform;
        _player.SetActive(false);
        StartCoroutine(BuildFirstRoad());
        StartCoroutine(BuildRoad());
    }
    private void Update()
    {
        if(transform.childCount != 0)
        {
            if (transform.GetChild(0).GetChild(0).transform.position.z < _cam.transform.position.z - _destroyDistance)
            {
                Destroy(transform.GetChild(0).GetChild(0).gameObject);
                _addRoad = true;
            }
        }
    }
    void BuildRoads(List<GameObject> instancia, int selection)
    {
        //build road
        GameObject road = Instantiate(instancia[selection], _newSpawnPoint) as GameObject;
        road.transform.parent = transform.GetChild(0);

        //set Color Road
        _colorRoad = (_colorRoad != 0) ? _colorRoad = 0 : _colorRoad = 1;

        //get new pos spawn
        _newSpawnPoint = road.transform.GetChild(road.transform.childCount - 1);
    }
    void InitAndEndRoad(List<GameObject> instancia, int secuencia, int limit, int first, int last)
    {
        if (secuencia == 0)
            BuildRoads(instancia, first);
        else if (secuencia == limit)
            BuildRoads(instancia, last);
        else
            BuildRoads(_straight, Random.Range(0, _straight.Count));
    }
    IEnumerator BuildFirstRoad()
    {
        for (int i = 0; i <= 6; i++)
        {
            BuildRoads(_straight, Random.Range(0, 3));
            yield return new WaitForSeconds(_buildTime / 2);
        }
        _addRoad = true;
        _player.SetActive(_addRoad);
        yield return null;
    }
    IEnumerator BuildRoad()
    {
        while (true)
        {
            _roadSection = Random.Range(0, 5);
            
            for (int i = 0; i < _lenthLimit; i++)
            {
                yield return new WaitUntil(() => _addRoad == true);
                int lastNum = _lenthLimit - 1;
                switch (_roadSection)
                {
                    case 0: BuildRoads(_straight, Random.Range(0, _straight.Count)); break;
                    case 1: InitAndEndRoad(_upDown, i, lastNum, 0, 1); break;
                    case 2: InitAndEndRoad(_upDown, i, lastNum, 1, 0); break;
                    case 3: InitAndEndRoad(_twistLeftRight, i, lastNum, 0, 1); break;
                    case 4: InitAndEndRoad(_twistLeftRight, i, lastNum, 1, 0); break;
                    default: break;
                }

                if (transform.GetChild(0).childCount >= _lenthLimit)
                    _addRoad = false;
            }
        }
    }
}