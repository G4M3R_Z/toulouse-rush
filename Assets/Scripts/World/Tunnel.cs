﻿using UnityEngine;

public class Tunnel : MonoBehaviour
{
    public GameObject _tunel;
    [Range(0,25)]
    public float _separation;
    private SpawnRoad _road;

    private void Awake()
    {
        _road = GameObject.FindGameObjectWithTag("World").GetComponent<SpawnRoad>();

        Vector3 pos = Vector3.zero;
        pos.z = 3.75f;
        
        for (int i = 0; i < _road._lenthLimit - 2; i++)
        {
            pos.x -= _separation;
            GameObject tunel = Instantiate(_tunel, transform) as GameObject;
            tunel.transform.localPosition = pos;
        }
    }
    private void Update()
    {
        if (transform.childCount != 0)
        {
            if (transform.GetChild(0).position.z < _road._cam.transform.position.z - _road._destroyDistance)
                Destroy(transform.GetChild(0).gameObject);
        }
        else
                Destroy(this.gameObject);
    }
}