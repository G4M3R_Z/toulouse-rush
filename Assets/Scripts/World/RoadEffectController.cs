﻿using System.Collections.Generic;
using UnityEngine;

public class RoadEffectController : MonoBehaviour
{
    [Range(0, 3)]
    public float _upDistance;
    [Range(0, 20)]
    public float _downDistance, _speed;
    public List<Material> _mats;

    [Range(0,180)]
    public float _pilarRotX, _pilarRotY, _pilarRotZ;
}