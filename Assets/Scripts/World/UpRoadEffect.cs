﻿using UnityEngine;

public class UpRoadEffect : MonoBehaviour
{
    private SpawnRoad _spawnRoad;
    private RoadEffectController _effect;
    private MeshRenderer _mesh;
    private float _yPos, _yUpPos;
    private bool _change;

    private void Awake()
    {
        _spawnRoad = GetComponentInParent<SpawnRoad>();
        _effect = GetComponentInParent<RoadEffectController>();
        
        _mesh = GetComponent<MeshRenderer>();
        _mesh.material = _effect._mats[_spawnRoad._colorRoad];

        _yPos = transform.localPosition.y;
        _yUpPos = _yPos + _effect._upDistance;

        float _move = _yPos - _effect._downDistance;
        transform.localPosition = new Vector3(transform.localPosition.x, _move, transform.localPosition.z);
    }
    private void FixedUpdate()
    {
        MoveRoad(transform.localPosition);
    }
    void MoveRoad(Vector3 pos)
    {
        Vector3 normalPos = new Vector3(pos.x, _yUpPos, pos.z);
        transform.localPosition = Vector3.Lerp(transform.localPosition, normalPos, Time.deltaTime * _effect._speed);

        if (Mathf.Abs(pos.y - normalPos.y) < 0.1f && !_change)
        {
            _yUpPos = _yPos;
            _change = true;
        }

        if (Mathf.Abs(pos.y - _yPos) < 0.01f && _change)
            Destroy(this);
    }
}