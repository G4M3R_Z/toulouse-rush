﻿using System.Collections;
using UnityEngine;

public class CloseSection : MonoBehaviour
{
    private SpawnRoad _road;
    private Score _progre;

    public GameObject _tunnel;
    [Range(0, 500)] public int _initSTime;
    [Range (0, 10)] public int _randomCreation;
    [HideInInspector] public bool _isTunnel;

    private void Start()
    {
        _road = GetComponent<SpawnRoad>();
        _progre = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Score>();
        StartCoroutine(BuildCloseSection());
    }

    IEnumerator BuildCloseSection()
    {
        yield return new WaitUntil(() => _progre._score >= _initSTime && _road._roadSection != 0);

        while (true)
        {
            yield return new WaitUntil(() => _road._roadSection == 0);

            int randomNum = Random.Range(0, _randomCreation);

            if(randomNum == _randomCreation / 2)
            {
                _isTunnel = true;
                Transform pos = _road._newSpawnPoint;
                GameObject tunel = Instantiate(_tunnel, pos.position, pos.rotation) as GameObject;
                tunel.transform.parent = transform.GetChild(1);
                tunel.transform.localEulerAngles = new Vector3(0, -90, 0);
            }

            yield return new WaitUntil(() => _road._roadSection != 0);

            _isTunnel = false;
        }
    }
}