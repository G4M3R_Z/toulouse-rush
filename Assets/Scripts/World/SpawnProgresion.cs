﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnProgresion : MonoBehaviour
{
    [Range(0, 500)]
    public int _progresiveValue;
    public List<GameObject> _dropElements;

    [HideInInspector] public int _obsLimit;
    private int _getScore;

    private Score _score;

    private void Awake()
    {
        _score = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Score>();

        _getScore = (int)_score._score + _progresiveValue;
        _obsLimit = 5;
    }
    private void Update()
    {
        if (_score._score >= _getScore && _dropElements.Count > _obsLimit)
        {
            _obsLimit++;
            _getScore = (int)_score._score + _progresiveValue;
        }
    }
}